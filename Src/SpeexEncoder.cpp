/*
 * SpeexEncoder.cpp
 *
 *  Created on: 23.07.2017
 *      Author: Slawomir Ciesielski
 */

#include "SpeexEncoder.h"
#include "stdio.h"

#include "stm32f7xx_hal.h"

SpeexEncoder::SpeexEncoder() {
	speexObj_ = speex_encoder_init(&speex_wb_mode);
	speex_bits_init(&speexBits_);
	int tmp = 5;
	speex_encoder_ctl(speexObj_, SPEEX_SET_QUALITY, &tmp);
	tmp = 1;
	speex_encoder_ctl(speexObj_, SPEEX_SET_DTX, &tmp);
}

SpeexEncoder::~SpeexEncoder() {
	// TODO Auto-generated destructor stub
}

std::vector<uint8_t> SpeexEncoder::encode(int16_t* data, std::size_t len) {
	std::vector<uint8_t> result(len, 0);

	std::size_t nbBytes = 0;
	std::size_t frameSize = 0;

	speex_encoder_ctl(speexObj_, SPEEX_GET_FRAME_SIZE, &frameSize);
	if (len % frameSize) {
		printf("WARN: some data will be lost during encoding!\n");
	}

	for (std::size_t i = 0; i < (len / frameSize); ++i)
	{
		speex_bits_reset(&speexBits_);
		speex_encode_int(speexObj_, reinterpret_cast<spx_int16_t*>(data + (i*frameSize)), &speexBits_);
		nbBytes += speex_bits_write(&speexBits_, reinterpret_cast<char*>(&result[nbBytes]), result.size()-nbBytes);
	}
	result.resize(nbBytes);

	return result;
}

std::size_t SpeexEncoder::getFrameSize() {
	std::size_t frameSize = 0;
	speex_encoder_ctl(speexObj_, SPEEX_GET_FRAME_SIZE, &frameSize);
	return frameSize;
}
