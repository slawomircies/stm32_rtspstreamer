/*
 * AudioRecorder.cpp
 *
 *  Created on: 19.07.2017
 *      Author: Slawomir Ciesielski
 */

#include <AudioDevice.h>

#include <stm32746g_discovery.h>
#include <stm32746g_discovery_audio.h>
#include <embxx/util/Assert.h>
#include <EventDispatcher.h>



int16_t AudioDevice::outBuff_[WriteQueueSize] = {0};
int16_t AudioDevice::inBuff_[ReadQueueSize] = {0};

AudioDevice::AudioDevice() :
		isPlaying_(false),
		isRecording_(false)
{
	// TODO Auto-generated constructor stub

}

AudioDevice::~AudioDevice() {
	// TODO Auto-generated destructor stub
}

void AudioDevice::init() {
	RCC_PeriphCLKInitTypeDef  PeriphClkInitStruct;
    PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_LTDC;
    PeriphClkInitStruct.PLLSAI.PLLSAIN = 192;
    PeriphClkInitStruct.PLLSAI.PLLSAIR = 5;
    PeriphClkInitStruct.PLLSAIDivR = RCC_PLLSAIDIVR_4;
    HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct);
    //BSP_AUDIO_IN_OUT_Init(INPUT_DEVICE_DIGITAL_MICROPHONE_2, OUTPUT_DEVICE_AUTO, AUDIO_FREQUENCY_8K, 0, 2);
    BSP_AUDIO_IN_Init(AUDIO_FREQUENCY_16K, 0, 1);
}

void AudioDevice::deinit() {
	BSP_AUDIO_IN_DeInit();
}

void AudioDevice::stopRecording() {
	isRecording_ = false;
	BSP_AUDIO_IN_Stop(CODEC_PDWN_HW);
	recordInfo_ = {};
}

void AudioDevice::stopPlaying() {
	isPlaying_ = false;
	BSP_AUDIO_OUT_Stop(CODEC_PDWN_HW);
	recordInfo_ = {};
}

void AudioDevice::startRead(int16_t* buff, std::size_t length)
{
	BSP_AUDIO_IN_Record((uint16_t*)buff, length);
}

void AudioDevice::startWrite(int16_t* buff, std::size_t length)
{
	BSP_AUDIO_OUT_Play((uint16_t*)buff, length * sizeof(int16_t));
}

void AudioDevice::writeCompleteHandler()
{
	if (isPlaying_)
	{
		playInfo_.start_ = outBuff_ + WriteQueueSize/2;

		GASSERT(playInfo_.handler_);
		playInfo_.handler_(playInfo_.start_, WriteQueueSize/2);
	}
}

void AudioDevice::writeHalfHandler()
{
	if (isPlaying_)
	{
		playInfo_.start_ = outBuff_;

		GASSERT(playInfo_.handler_);
		playInfo_.handler_(playInfo_.start_, WriteQueueSize/2);
	}
}

void AudioDevice::readCompleteHandler()
{
	if (isRecording_)
	{
		recordInfo_.start_ = inBuff_ + ReadQueueSize/2;

		GASSERT(recordInfo_.handler_);
		recordInfo_.handler_(recordInfo_.start_, ReadQueueSize/2);
	}
}

void AudioDevice::readHalfHandler()
{
	if (isRecording_)
	{
		recordInfo_.start_ = inBuff_;

		GASSERT(recordInfo_.handler_);
		recordInfo_.handler_(recordInfo_.start_, ReadQueueSize/2);
	}
}

void AudioDevice::handleEvent(int evt) {
	switch (evt)
	{
	case AUDIO_EVT_RXCPLT:
		readCompleteHandler();
		break;
	case AUDIO_EVT_RXHALF:
		readHalfHandler();
		break;

	case AUDIO_EVT_TXCPLT:
		writeCompleteHandler();
		break;

	case AUDIO_EVT_TXHALF:
		writeHalfHandler();
		break;
	default:
		break;
	}
}

extern SAI_HandleTypeDef haudio_out_sai;

/**
  * @brief This function handles DMA2 Stream 4 interrupt request.
  * @param None
  * @retval None
  */
extern "C" void AUDIO_OUT_SAIx_DMAx_IRQHandler(void)
{
	HAL_DMA_IRQHandler(haudio_out_sai.hdmatx);
}


extern SAI_HandleTypeDef haudio_in_sai;

/**
  * @brief This function handles DMA2 Stream 7 interrupt request.
  * @param None
  * @retval None
  */
extern "C" void AUDIO_IN_SAIx_DMAx_IRQHandler(void)
{
	HAL_DMA_IRQHandler(haudio_in_sai.hdmarx);
}

extern "C" void BSP_AUDIO_IN_TransferComplete_CallBack(void)
{
	EventDispatcher::pushAudioEvt(AudioDevice::AUDIO_EVT_RXCPLT);
}

extern "C" void BSP_AUDIO_IN_HalfTransfer_CallBack(void)
{
	EventDispatcher::pushAudioEvt(AudioDevice::AUDIO_EVT_RXHALF);
}

extern "C" void BSP_AUDIO_OUT_TransferComplete_CallBack(void)
{
	EventDispatcher::pushAudioEvt(AudioDevice::AUDIO_EVT_TXCPLT);
}

extern "C" void BSP_AUDIO_OUT_HalfTransfer_CallBack(void)
{
	EventDispatcher::pushAudioEvt(AudioDevice::AUDIO_EVT_TXHALF);
}

