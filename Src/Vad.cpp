/*
 * Vad.cpp
 *
 *  Created on: 31.08.2017
 *      Author: Slawomir Ciesielski
 */

#include "Vad.h"

#include <algorithm>
#include <numeric>

Vad::Vad() :
	trainCnt_(0),
	hangOverNs_(0),
	hangOverS_(0),
	hangOverNsCnt_(5),
	hangOverSCnt_(20)
{
	// TODO Auto-generated constructor stub

}

Vad::~Vad() {
	// TODO Auto-generated destructor stub
}

void Vad::init() {
	arm_rfft_fast_init_f32(&S_, 512);
	Bns_.clear();
	Bs_.clear();
	trainCnt_ = 0;
	hangOverNs_ = 0;
	hangOverS_ = 0;
	thr_ = 0.0;
	prevVad_ = false;
}

bool Vad::hangOver(bool vad) {
	bool result = vad;

	if (vad == true && hangOverNs_ == 0)
	{
		hangOverS_ = hangOverSCnt_;
	}
	else if (prevVad_ == true && vad == false && hangOverS_ == 0)
	{
		hangOverNs_ = hangOverNsCnt_;
	}
	else if (vad == true && hangOverNs_ > 0)
	{
		result = false;
		hangOverNs_ = hangOverNs_ - 1;
	}
	else if (vad == false && hangOverS_ > 0)
	{
		result = true;
		hangOverS_ = hangOverS_ - 1;
	}
	else
	{
		hangOverNs_ > 0 ? --hangOverNs_ : hangOverNs_;
		hangOverS_  > 0 ? --hangOverS_  : hangOverS_;
	}

	prevVad_ = result;

	return result;
}

void Vad::updateThr(bool vad, float VD) {

	if (vad == false)
	{
		Bns_.pop_front(1);
		Bns_.push_back(VD);
	}
	else
	{
		Bs_.pop_front(1);
		Bs_.push_back(VD);
	}

	float tmp;
	uint32_t tmpIdx;
	float meanS_1 = 0;
	float meanS_2 = 0;
	uint32_t meanS_1_dist;
	uint32_t meanS_2_dist;

	float meanS = 0;
	float maxNs = 0;

	auto range = Bns_.arrayOne();
	auto distance = static_cast<std::size_t>(
			std::distance(range.first, range.second));

	arm_max_f32(range.first, distance, &tmp, &tmpIdx);

	range = Bns_.arrayTwo();
	distance = static_cast<std::size_t>(
			std::distance(range.first, range.second));

	if (distance > 0)
		arm_max_f32(range.first, distance, &maxNs, &tmpIdx);
	else
		maxNs = tmp;

	maxNs = std::max(maxNs, tmp);

	range = Bs_.arrayOne();
	meanS_1_dist = static_cast<std::size_t>(
			std::distance(range.first, range.second));

	arm_mean_f32(range.first, meanS_1_dist, &meanS_1);

	range = Bs_.arrayTwo();
	meanS_2_dist = static_cast<std::size_t>(
			std::distance(range.first, range.second));

	if (meanS_2_dist > 0)
		arm_mean_f32(range.first, meanS_2_dist, &meanS_2);

	meanS = (meanS_1 * meanS_1_dist + meanS_2 * meanS_2_dist)
			/(meanS_1_dist + meanS_2_dist);

//	std::vector<float> sortedS(10);
//
//	for (auto&& s : sortedS)
//	{
//		s = Bs_.front();
//		Bs_.popFront(1);
//		Bs_.pushBack(s);
//	}
//
//	std::sort(sortedS.begin(), sortedS.end());

	thr_ = 0.3 * meanS + (1-0.3) * maxNs;
}

void Vad::initThr() {
	float mean;
	float std;

	Bns_.linearise();

	auto rangeOne = Bns_.arrayOne();
	auto distance = static_cast<std::size_t>(std::distance(rangeOne.first,
			rangeOne.second));

	arm_mean_f32(&Bns_.front(), distance, &mean);
	//arm_std_f32(&Bns_.front(), distance, &std);

	std::vector<float> sortedNS(distance);

	for (auto&& ns : sortedNS)
	{
		ns = *(rangeOne.first++);
	}

	std::sort(sortedNS.begin(), sortedNS.end());

	size_t medIdx = (sortedNS.size() * 50) / 100;
	size_t per80Idx = (sortedNS.size() * 80) / 100;

	//thr_ = mean + .1 * std;
	thr_ = sortedNS[medIdx] + 1 * sortedNS[per80Idx];

	while (!Bns_.full())
		Bns_.push_back(thr_);

	while (!Bs_.full())
		Bs_.push_back(thr_);
}

float Vad::computeVD(int16_t* data, std::size_t len)
{
	static std::vector<float> input(512);
	static std::vector<float> output(512);
	static std::vector<float> db12 = {
			-0.013112258, 0.10956627,
			-0.37735513, 0.65719873,
			-0.51588649, -0.044763885,
			0.31617844, -0.023779258,
			-0.18247861, 0.0053595696,
			0.096432120, 0.010849130,
			-0.041546278, -0.012218649,
			0.012840825, 0.0067114988,
			-0.0022486073, -0.0021795037,
			-6.5451281e-06, 0.00038865308,
			8.8504108e-05, -2.4241546e-05,
			-1.2776953e-05, -1.5290717e-06
	};

	float result = 0.0;

	const size_t beginIndex = 2;
	const size_t endIndex = 100;

	static std::vector<float> convoutput(24 + (endIndex - beginIndex) -1);

	arm_q15_to_float(data, input.data(), len);
	std::fill(input.begin()+len,input.end(), 0.0);

	arm_rfft_fast_f32(&S_, input.data(), output.data(), 0);
	arm_cmplx_mag_f32(output.data() + (beginIndex * 2), output.data(), (endIndex - beginIndex));

	arm_conv_f32(db12.data(), db12.size(), output.data(), (endIndex - beginIndex), convoutput.data());

	arm_mult_f32(convoutput.data(), convoutput.data(), convoutput.data(), convoutput.size());

	result = std::accumulate(convoutput.begin(), convoutput.end(), result);

	return result;
}

bool Vad::process(int16_t* data, std::size_t len, float *param) {
	bool result = false;
	bool vad = false;

	float VD;

	VD = computeVD(data, len);

	if (trainCnt_ == trainTime_)
	{
		initThr();
		++trainCnt_;
	}
	else if (trainCnt_ < trainTime_)
	{
		vad 	= false;
		result 	= false;
		Bns_.pushBack(VD);
		++trainCnt_;
	}
	else
	{
		vad = VD > thr_;

	    result = hangOver(vad);

	    updateThr(vad, VD);
	}

	if (nullptr != param)
		*param = VD;

	return result;
}

float Vad::getThr() const
{
	return thr_;
}
