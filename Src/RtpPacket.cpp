/*
 * rtpPacket.cpp
 *
 *  Created on: 29.05.2017
 *      Author: Slawomir Ciesielski
 */

#include <RtpPacket.h>

#include <stm32f746xx.h>
#include <core_cm7.h>
#include <core_cmInstr.h>

#include <algorithm>



RtpPacket::RtpPacket()
{

}

RtpPacket::~RtpPacket()
{

}

void RtpPacket::setHeader(std::vector<uint32_t>& data)
{
	std::copy_n(data.begin(), 3, header_);
}

uint8_t* RtpPacket::getHeader()
{
	return reinterpret_cast<uint8_t*>(header_);
}

uint32_t RtpPacket::getVersion()
{
	return bitfield::get<6, 2>(header_[0]);
}

void RtpPacket::setVersion(uint32_t val)
{
	return bitfield::set<6, 2>(header_[0], val);
}

uint32_t RtpPacket::getPadding()
{
	return bitfield::get<5, 1>(header_[0]);
}

void RtpPacket::setPadding(uint32_t val)
{
	return bitfield::set<5, 1>(header_[0], val);
}

uint32_t RtpPacket::geteXtension()
{
	return bitfield::get<4, 1>(header_[0]);
}

void RtpPacket::seteXtension(uint32_t val)
{
	return bitfield::set<4, 1>(header_[0], val);
}

uint32_t RtpPacket::getCsrcCount()
{
	return bitfield::get<0, 4>(header_[0]);
}

void RtpPacket::setCsrcCount(uint32_t val)
{
	return bitfield::set<0, 4>(header_[0], val);
}

uint32_t RtpPacket::getMarker()
{
	return bitfield::get<15, 1>(header_[0]);
}

void RtpPacket::setMarker(uint32_t val)
{
	return bitfield::set<15, 1>(header_[0], val);
}

uint32_t RtpPacket::getPayloadType()
{
	return bitfield::get<8, 7>(header_[0]);
}

void RtpPacket::setPayloadType(uint32_t val)
{
	return bitfield::set<8, 7>(header_[0], val);
}

uint32_t RtpPacket::getSequenceNumber()
{
	return __REV16(bitfield::get<16, 16>(header_[0]));
}

void RtpPacket::setSequenceNumber(uint32_t val)
{
	return bitfield::set<16, 16>(header_[0], __REV16(val));
}

uint32_t RtpPacket::getTimeStamp()
{
	return __REV(header_[1]);
}

void RtpPacket::setTimeStamp(uint32_t val)
{
	header_[1] = __REV(val);
}

uint32_t RtpPacket::getSSRC()
{
	return __REV(header_[2]);
}

void RtpPacket::setSSRC(uint32_t val)
{
	header_[2] = __REV(val);
}
