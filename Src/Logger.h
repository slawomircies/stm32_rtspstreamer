/*
 * Logger.h
 *
 *  Created on: 26.08.2017
 *      Author: Slawomir Ciesielski
 */

#ifndef LOGGER_H_
#define LOGGER_H_

#include <embxx/container/StaticQueue.h>
#include <cmsis_os.h>

class Logger {
public:
	Logger();
	~Logger();

	void run();
	void stop();

	size_t write(const uint8_t* msg, size_t len);
	void written();

private:
	void remove(size_t len);
	enum EvtType
	{
		EVT_STOP,
		EVT_TX_REQ,
		EVT_TX_DONE
	};

	struct evt_t {
		EvtType type;
	};

	size_t pendingReqSize_;

	osMailQDef(logger_mail_box_, 20, evt_t);
	static osMailQId  logger_mail_box_;

	osMutexDef(logger_mutex_);
	static osMutexId logger_mutex_;

	embxx::container::StaticQueue<uint8_t, 2048> buff_;
};

#endif /* LOGGER_H_ */
