/*
 * Timer.cpp
 *
 *  Created on: 17.07.2017
 *      Author: Slawomir Ciesielski
 */

#include <Timer.h>
#include <EventDispatcher.h>

Timer::Timer()
{

}

Timer::~Timer()
{

}

void Timer::stop()
{
	osTimerStop(timer_);
	osTimerDelete(timer_);
}

void Timer::execute()
{
	GASSERT(handler_);
	handler_();
}

void Timer::timeout()
{
	EventDispatcher::pushTimEvt(this);
}

void Timer::timeout(const void* obj)
{
	if (nullptr != obj)
	{
		reinterpret_cast<Timer*>(pvTimerGetTimerID(const_cast<TimerHandle_t>(obj)))->timeout();
	}

	//ConnectionMgr::pushTimEvt(reinterpret_cast<Timer*>(const_cast<void*>(obj)));
	//printf("\ntimer!\n\n");
}
