/*
 * AudioGenerator.cpp
 *
 *  Created on: 13.07.2017
 *      Author: Slawomir Ciesielski
 */

#include "AudioGenerator.h"


AudioGenerator::AudioGenerator()
{

}

AudioGenerator::~AudioGenerator()
{

}

void AudioGenerator::timeouts() {

	size_t nbBytes = 0;
	std::vector<uint8_t> cbits(100,0);
	speex_bits_reset(&speexBits_);
	speex_encode_int(speexObj_, reinterpret_cast<spx_int16_t*>(&sin200[0]), &speexBits_);
	for (int i=0; i<1; i++)
	{
		nbBytes += speex_bits_write(&speexBits_, reinterpret_cast<char*>(&cbits[nbBytes]), cbits.size()-nbBytes);
	}
    cbits.resize(nbBytes);
	handler_(cbits);
}



