/*
 * RtspSession.cpp
 *
 *  Created on: 10.07.2017
 *      Author: Slawomir Ciesielski
 */

#include "RtspSession.h"
#include "RtcpPacket.h"
#include "RtpPacket.h"

RtspSession::RtspSession():
	state_(State::INIT),
	rtcpPort_(0),
	rtpPort_(0),
	packetsSent_(0),
	octetsSent_(0),
	connRtp_(0),
	connRtcp_(0)
{
	// TODO Auto-generated constructor stub

}

RtspSession::~RtspSession() {
	// TODO Auto-generated destructor stub
}

void RtspSession::setup(UdpConnection* connRtp, UdpConnection* connRtcp) {
	connRtp_ = connRtp;
	connRtcp_ = connRtcp;
}

void RtspSession::stop() {
	packetsSent_ = 0;
	octetsSent_ = 0;
	state_ = State::INIT;
}

void RtspSession::processRTCP(netbuf* buf) {
	pbuf* p = buf ? buf->ptr : 0;
	printf("Received RTCP packet (%d bytes)\n", p ? p->tot_len : 0);

	if ((nullptr != p) && (p->tot_len > 0) && ((p->tot_len & 0x03) == 0x00))
	{
		size_t processedBytes = 0;

		while (processedBytes < p->tot_len)
		{
			RtcpPacket header;
			pbuf_copy_partial(p, reinterpret_cast<uint8_t *>(&header), sizeof(header), processedBytes);

			printf("Version: %u\n", 		header.getVersion());
			printf("Padding: %u\n", 		header.getPadding());
			printf("Reception Count: %u\n", header.getCount());
			printf("PayloadType: %u\n", 	header.getPayloadType());
			printf("Length: %u\n", 			header.getLength());

			err_t err = ERR_OK;

			if (header.getVersion() != 2)
			{
				printf("UNSUPPORTED FRAME!\n");
				err = ERR_BUF;
			}
			if (header.getLength() > (p->tot_len - processedBytes))
			{
				printf("INVALID FRAME LENGTH!\n");
				err = ERR_BUF;
			}

			if (ERR_OK != err)
			{
				break;
			}

			std::vector<uint32_t> data(header.getLength()+1, 0);

			pbuf_copy_partial(p, reinterpret_cast<uint8_t *>(&data.front()), data.size() * sizeof(uint32_t), processedBytes);
			processedBytes += data.size() * sizeof(uint32_t);

			RtcpPacket* packet = reinterpret_cast<RtcpPacket*>(data.data());

			switch (packet->getPayloadType())
			{
			case 200:
				{
					 printf("SR - sender report - NOT EXPECTED\n");
				}
				break;
			case 201:
				{
					printf("RR - receiver report\n");

					printf("SSRC: 0x%x\n", packet->getSSRCSender());

					RtcpReportBlock* rec_report = packet->getReportBlock();
					if (nullptr != rec_report){
						printf("Fraction lost: %u\n", 		rec_report->getFractLost());
						printf("Packets lost: %u\n", 		rec_report->getPacketsLost());
						printf("Highest sequence number received: %u\n", rec_report->getHiSeqRcvd());
						printf("Jitter: %u\n", 				rec_report->getJitter());
						printf("Last SR: %u\n", 			rec_report->getLastSR());
						printf("Delay since last SR: %u\n", rec_report->getDelayLastSR());
					}
				}
				break;
			case 202:
				{
					printf("SDES - source description\n");

					RtcpSourceDescription* src_description = packet->getSourceDescription();
					if (nullptr != src_description){
						printf("SSRC: 0x%x\n", src_description->getSSRC());
						RtcpSourceDescriptionItem* item = src_description->getItem();
						while (nullptr != item && item->getType() != 0)
						{
							std::string itemData(reinterpret_cast<char*>(item->getData()), item->getLength());
							printf("ITEM:\n");
							printf("Type: %d\n", 	item->getType());
							printf("Length: %d\n", 	item->getLength());
							printf("Data: %s\n", 	itemData.c_str());

							item = item->getItemNext();
						}
					}
				}
				break;
			case 203:
				{
					printf("BYE - goodbye\n");
					stop();
				}
				break;
			case 204:
				{
					printf("APP - application-defined - NOT EXPECTED\n");
				}
				break;
			}
		}
	}
	else
	{
		printf("Invalid data length!\n");
	}
}

void RtspSession::processRTP(netbuf* buf) {
	pbuf* p = buf ? buf->ptr : 0;
	printf("Received RTP packet (%d bytes)\n", p ? p->tot_len : 0);

	if (nullptr != p && p->tot_len >= 4)
	{
		std::vector<uint32_t> data(1,0);

		pbuf_copy_partial(p, reinterpret_cast<uint8_t *>(&data[0]), 4, 0);

		RtpPacket packet;
		packet.setHeader(data);

		printf("Version: %d\n", packet.getVersion());
		printf("Padding: %d\n", packet.getPadding());
		printf("eXtension: %d\n", packet.geteXtension());
		printf("CsrcCount: %d\n", packet.getCsrcCount());
		printf("Marker: %d\n", packet.getMarker());
		printf("Payload type: %d\n", packet.getPayloadType());
		printf("SequenceNumber: %d\n", packet.getSequenceNumber());
	}
}

void RtspSession::pushData(const std::vector<uint8_t>& data) {

//	sendRtpFrame(data);
	sendRtcpReport();
}

void RtspSession::sendRtpFrame(const std::vector<uint8_t>& data) {
	if (state_ == State::PLAYING)
	{
		if (packetsSent_ % 50 == 0 || packetsSent_ == 0)
		{
			sendRtcpReport();
		}

		//printf("Sending RTP frame (%d bytes)\n", data.size());

		RtpPacket packet;

		packet.setVersion(2);
		packet.setPadding(0); // false
		packet.seteXtension(0); // false
		packet.setCsrcCount(0); // none
		packet.setMarker(1);
		packet.setPayloadType(97); // DynamicRTP-Type-97
		packet.setSequenceNumber(packetsSent_+1);
		packet.setTimeStamp(((packetsSent_) * 320));
		packet.setSSRC(0x11223344);

		std::vector<char> p(data.size()+sizeof(RtpPacket),0);
		std::copy(reinterpret_cast<char*>(&packet), reinterpret_cast<char*>(&packet)+sizeof(RtpPacket), p.begin());
		std::copy(data.begin(), data.end(), p.begin()+sizeof(RtpPacket));
		octetsSent_ += data.size();

		connRtp_->send(const_cast<const char*>(&p.front()), p.size());

		++packetsSent_;
	}
	else
	{
		printf("RtspSession::sendRtpFrame: Session not in PLAYing state!\n");
	}
}

void RtspSession::sendRtcpReport() {
	if (state_ == State::PLAYING)
	{
		printf("Sending RTCP report\n");

		RtcpPacket packet;
		RtcpSenderInfo sender_info;

		uint32_t timestamp = HAL_GetTick();

		ntp_t ntpTimestamp = convertToNtp(timestamp);

		packet.setVersion(2);
		packet.setPadding(0); // false
		packet.setCount(0);
		packet.setPayloadType(200); // Sender Report
		packet.setLength(6);
		packet.setSSRCSender(0x11223344);

		sender_info.setNTPTimestampHi(ntpTimestamp.intpart);
		sender_info.setNTPTimestampLo(ntpTimestamp.fractpart);
		sender_info.setRTPTimestamp(((packetsSent_) * 320));
		sender_info.setPacketCount(packetsSent_);
		sender_info.setOctetCount(octetsSent_);


		std::vector<char> p(sizeof(RtcpPacket) + sizeof(RtcpSenderInfo),0);
		std::copy(reinterpret_cast<char*>(&packet), reinterpret_cast<char*>(&packet)+sizeof(RtcpPacket), p.begin());
		std::copy(reinterpret_cast<char*>(&sender_info), reinterpret_cast<char*>(&sender_info)+sizeof(RtcpSenderInfo), p.begin()+sizeof(RtcpPacket));

		connRtcp_->send(const_cast<const char*>(&p.front()), p.size());
	}
	else
	{
		printf("RtspSession::sendRtpFrame: Session not in PLAYing state!\n");
	}
}

RtspSession::ntp_t RtspSession::convertToNtp(uint32_t milliseconds)
{
	ntp_t result;

	result.intpart = milliseconds / 1000;
	result.fractpart = ((milliseconds % 1000) * 0x100000000L) / 1000;

	return result;
}

RtspSession::State RtspSession::getState() {
	return state_;
}

void RtspSession::setState(const State& state) {
	state_ = state;
}
