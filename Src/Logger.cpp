/*
 * Logger.cpp
 *
 *  Created on: 26.08.2017
 *      Author: Slawomir Ciesielski
 */

#include "Logger.h"
#include "stm32f7xx_hal.h"


osMailQId Logger::logger_mail_box_;
osMutexId Logger::logger_mutex_;

extern UART_HandleTypeDef huart1;

Logger::Logger():
		pendingReqSize_(0)
{
	// TODO Auto-generated constructor stub
}

Logger::~Logger() {
	// TODO Auto-generated destructor stub
}

void Logger::run() {
	bool running = true;

	logger_mail_box_ = osMailCreate(osMailQ(logger_mail_box_), NULL);
	logger_mutex_ = osMutexCreate(osMutex(logger_mutex_));

    while (running) {
        osEvent evt = osMailGet(logger_mail_box_, osWaitForever);
        if (evt.status == osEventMail) {
        	evt_t *mail = (evt_t*)evt.value.p;

        	switch (mail->type)
        	{
        	case EVT_TX_REQ:
        	{
        		if (0 == pendingReqSize_)
        		{
					osMutexWait(logger_mutex_, osWaitForever);
					auto rangeOne = buff_.arrayOne();
					osMutexRelease(logger_mutex_);

					auto distance = static_cast<std::size_t>(
						std::distance(rangeOne.first, rangeOne.second));

					pendingReqSize_ = distance;

					if (distance > 0) {
						HAL_UART_Transmit_IT(&huart1, rangeOne.first, pendingReqSize_);
					}
        		}
        		break;
        	}
        	case EVT_TX_DONE:
        	{
        		remove(pendingReqSize_);
        		pendingReqSize_ = 0;

        		evt_t *mail = (evt_t*)osMailAlloc(logger_mail_box_, osWaitForever);
        		mail->type = EVT_TX_REQ;
        		osMailPut(logger_mail_box_, mail);

        		break;
        	}
        	case EVT_STOP:
        	{
        		running = false;
        		break;
        	}
        	default:
        	{
        		running = false;
        	}
        	}

            osMailFree(logger_mail_box_, mail);
        }
    }

}

void Logger::stop() {
	evt_t *mail = (evt_t*)osMailAlloc(logger_mail_box_, osWaitForever);

	mail->type = EVT_STOP;

	osMailPut(logger_mail_box_, mail);
}

size_t Logger::write(const uint8_t* msg, size_t len) {
	size_t result = 0;

    osMutexWait(logger_mutex_, osWaitForever);
	if ((buff_.capacity()-buff_.size()) >= len)
	{
		for(size_t i = 0; i < len; ++i)
		{
			buff_.pushBack(msg[i]);
		}

		result = len;
	}
    osMutexRelease(logger_mutex_);

	evt_t *mail = (evt_t*)osMailAlloc(logger_mail_box_, osWaitForever);
	mail->type = EVT_TX_REQ;
	osMailPut(logger_mail_box_, mail);

    return result;
}

void Logger::written() {
	evt_t *mail = (evt_t*)osMailAlloc(logger_mail_box_, osWaitForever);

	mail->type = EVT_TX_DONE;

	osMailPut(logger_mail_box_, mail);
}

void Logger::remove(size_t len) {
    osMutexWait(logger_mutex_, osWaitForever);
	buff_.popFront(len);
    osMutexRelease(logger_mutex_);
}
