/*
 * rtcpPacket.cpp
 *
 *  Created on: 29.05.2017
 *      Author: Slawomir Ciesielski
 */



#include <RtcpPacket.h>

#include <stm32f746xx.h>
#include <core_cm7.h>
#include <core_cmInstr.h>



RtcpPacket::RtcpPacket()
{

}

RtcpPacket::~RtcpPacket()
{

}

void RtcpPacket::setHeader(uint32_t data)
{
	header_[0] = data;
}

uint8_t* RtcpPacket::getHeader()
{
	return reinterpret_cast<uint8_t*>(header_[0]);
}

uint32_t RtcpPacket::getVersion()
{
	return bitfield::get<6, 2>(header_[0]);
}

void RtcpPacket::setVersion(uint32_t val)
{
	bitfield::set<6, 2>(header_[0], val);
}

uint32_t RtcpPacket::getPadding()
{
	return bitfield::get<5, 1>(header_[0]);
}

void RtcpPacket::setPadding(uint32_t val)
{
	bitfield::set<5, 1>(header_[0], val);
}

uint32_t RtcpPacket::getCount()
{
	return bitfield::get<0, 5>(header_[0]);
}

void RtcpPacket::setCount(uint32_t val)
{
	bitfield::set<0, 5>(header_[0], val);
}

uint32_t RtcpPacket::getPayloadType()
{
	return bitfield::get<8, 8>(header_[0]);
}

void RtcpPacket::setPayloadType(uint32_t val)
{
	bitfield::set<8, 8>(header_[0], val);
}

uint32_t RtcpPacket::getLength()
{
	return __REV16(bitfield::get<16, 16>(header_[0]));
}

void RtcpPacket::setLength(uint32_t val)
{
	bitfield::set<16, 16>(header_[0], __REV16(val));
}

uint32_t RtcpPacket::getSSRCSender()
{
	return __REV(header_[1]);
}

void RtcpPacket::setSSRCSender(uint32_t val)
{
	header_[1] = __REV(val);
}

RtcpReportBlock* RtcpPacket::getReportBlock()
{
	if (getLength() > 6 && getPayloadType() == 200)
	{
		return reinterpret_cast<RtcpReportBlock*>(&header_[0]+7);
	}
	else if (getLength() > 1 && getPayloadType() == 201)
	{
		return reinterpret_cast<RtcpReportBlock*>(&header_[0]+2);
	}
	else
	{
		return 0;
	}
}

RtcpSenderInfo* RtcpPacket::getSenderInfo()
{
	if (getLength() > 2 && getPayloadType() == 200)
	{
		return reinterpret_cast<RtcpSenderInfo*>(&header_[0]+2);
	}
	else
	{
		return 0;
	}
}

RtcpSourceDescription*	RtcpPacket::getSourceDescription()
{
	if (getPayloadType() == 202)
	{
		return reinterpret_cast<RtcpSourceDescription*>(&header_[0]+1);
	}
	else
	{
		return 0;
	}
}

/********************* RtcpSourceDescription */

uint32_t RtcpSourceDescription::getSSRC()
{
	return __REV(data_[0]);
}

void RtcpSourceDescription::setSSRC(uint32_t val)
{
	data_[0] = __REV(val);
}


RtcpSourceDescriptionItem* RtcpSourceDescription::getItem()
{
	 RtcpSourceDescriptionItem* it = reinterpret_cast<RtcpSourceDescriptionItem*>(&data_[0]+1);
	 return it;
}

 /********************* RtcpSourceDescriptionItem */

uint8_t RtcpSourceDescriptionItem::getType()
{
	return data_[0];
}

void RtcpSourceDescriptionItem::setType(uint8_t val)
{
	data_[0] = val;
}


uint8_t RtcpSourceDescriptionItem::getLength()
{
	return data_[1];
}

void RtcpSourceDescriptionItem::setLength(uint8_t val)
{
	data_[1] = val;
}


uint8_t* RtcpSourceDescriptionItem::getData()
{
	return data_+2;
}

void RtcpSourceDescriptionItem::setData(uint8_t* val, size_t len)
{
	std::copy(val, val+len, data_+2);
}


RtcpSourceDescriptionItem* RtcpSourceDescriptionItem::getItemNext()
{
	RtcpSourceDescriptionItem* it = reinterpret_cast<RtcpSourceDescriptionItem*>(&data_[0] + 2 + getLength());
	return it;
}

/********************* RtcpSenderInfo */

uint32_t RtcpSenderInfo::getNTPTimestampHi()
{
	return __REV(data_[0]);
}

void RtcpSenderInfo::setNTPTimestampHi(uint32_t val)
{
	data_[0] = __REV(val);
}

uint32_t RtcpSenderInfo::getNTPTimestampLo()
{
	return __REV(data_[1]);
}

void RtcpSenderInfo::setNTPTimestampLo(uint32_t val)
{
	data_[1] = __REV(val);
}

uint32_t RtcpSenderInfo::getRTPTimestamp()
{
	return __REV(data_[2]);
}

void RtcpSenderInfo::setRTPTimestamp(uint32_t val)
{
	data_[2] = __REV(val);
}

uint32_t RtcpSenderInfo::getPacketCount()
{
	return __REV(data_[3]);
}

void RtcpSenderInfo::setPacketCount(uint32_t val)
{
	data_[3] = __REV(val);
}

uint32_t RtcpSenderInfo::getOctetCount()
{
	return __REV(data_[4]);
}

void RtcpSenderInfo::setOctetCount(uint32_t val)
{
	data_[4] = __REV(val);
}

/********************* RtcpReportBlock */

uint32_t RtcpReportBlock::getSSRC()
{
	return __REV(data_[0]);
}

void RtcpReportBlock::setSSRC(uint32_t val)
{
	data_[0] = __REV(val);
}


uint32_t RtcpReportBlock::getFractLost()
{
	return bitfield::get<0, 8>(data_[1]);
}

void RtcpReportBlock::setFractLost(uint32_t val)
{
	bitfield::set<0, 8>(data_[1], val);
}


uint32_t RtcpReportBlock::getPacketsLost()
{
	return __REV(bitfield::get<8, 24>(data_[1])) >> 8;
}

void RtcpReportBlock::setPacketsLost(uint32_t val)
{
	bitfield::set<8, 24>(data_[1], __REV(val)>>8);
}


uint32_t RtcpReportBlock::getHiSeqRcvd()
{
	return __REV(data_[2]);
}

void RtcpReportBlock::setHiSeqRcvd(uint32_t val)
{
	data_[2] = __REV(val);
}


uint32_t RtcpReportBlock::getJitter()
{
	return __REV(data_[3]);
}

void RtcpReportBlock::setJitter(uint32_t val)
{
	data_[3] = __REV(val);
}

uint32_t RtcpReportBlock::getLastSR()
{
	return __REV(data_[4]);
}

void RtcpReportBlock::setLastSR(uint32_t val)
{
	data_[4] = __REV(val);
}


uint32_t RtcpReportBlock::getDelayLastSR()
{
	return __REV(data_[5]);
}

void RtcpReportBlock::setDelayLastSR(uint32_t val)
{
	data_[5] = __REV(val);
}

RtcpReportBlock* RtcpReportBlock::getReportBlockNext()
{
	//TODO:implement multiple resource block handling
	return 0;
}
