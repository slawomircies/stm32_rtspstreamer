/*
 * TcpConnection.cpp
 *
 *  Created on: 11.07.2017
 *      Author: Slawomir Ciesielski
 */

#include "ConnMgr/TcpConnection.h"
#include <lwip/tcp.h>
#include <lwip/api.h>

TcpConnection::TcpConnection(ConnectionMgr& connMgr) :
	connMgr_(connMgr)
{
	// TODO Auto-generated constructor stub

}

TcpConnection::TcpConnection(ConnectionMgr& connMgr, netconn* conn) :
	Connection(conn),
	connMgr_(connMgr)
{
	// TODO Auto-generated constructor stub

}

TcpConnection::~TcpConnection() {
	// TODO Auto-generated destructor stub
}

err_t TcpConnection::connect(ip_addr_t ip, uint16_t port) {
	err_t result = netconn_connect(conn_, &ip, port);
	setState((result == ERR_OK) ? State::CONNECTED : State::DISCONECTED);
	return result;
}

err_t TcpConnection::disconnect() {
	err_t result = netconn_close(conn_);
	setState((result == ERR_OK) ? State::DISCONECTED: State::INVALID);
	return result;
}

std::size_t TcpConnection::send(const char* buf, std::size_t size) {
	std::size_t result = 0;
	netconn_write_partly(conn_, buf, size, NETCONN_COPY, &result);
	printf("Sent %d of %d bytes\n", result, size);
	return result;
}

void TcpConnection::fetchData() {
	netbuf* buf = nullptr;
	netconn_recv(conn_, &buf);

	GASSERT(recvHandler_);
	recvHandler_(buf);
	netbuf_delete(buf);
}

ip_addr_t TcpConnection::getRemoteIp() const {
	return conn_->pcb.tcp->remote_ip;
}

uint16_t TcpConnection::getRemotePort() const {
	return conn_->pcb.tcp->remote_port;
}

ip_addr_t TcpConnection::getLocalIp() const {
	return conn_->pcb.tcp->local_ip;
}

uint16_t TcpConnection::getLocalPort() const {
	return conn_->pcb.tcp->local_port;
}
