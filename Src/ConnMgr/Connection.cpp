/*
 * Conenction.cpp
 *
 *  Created on: 11.07.2017
 *      Author: Slawomir Ciesielski
 */


#include "ConnMgr/Connection.h"

Connection::Connection() :
	conn_(nullptr),
	state_(State::INVALID)
{
}

Connection::Connection(netconn* conn) :
	conn_(conn),
	state_(State::INVALID)
{
}

Connection::Connection(const Connection& o) :
	conn_(o.conn_),
	state_(o.state_)
{
}

Connection& Connection::operator=(Connection&& o) {
	conn_ = o.conn_;
	state_ = o.state_;
}

Connection::~Connection() {
	netconn_close(conn_);
	netconn_delete(conn_);
	conn_ = nullptr;
}

Connection::State Connection::getState() const{
	return state_;
}

void Connection::setState(State state) {
	state_ = state;
}

netconn* Connection::getNetconn() const {
	return conn_;
}

