/*
 * ConnectionMgr.cpp
 *
 *  Created on: 11.07.2017
 *      Author: Slawomir Ciesielski
 */

#include "ConnMgr/ConnectionMgr.h"
#include "ConnMgr/TcpConnection.h"
#include "ConnMgr/UdpConnection.h"
#include "ConnMgr/TcpListener.h"

#include <EventDispatcher.h>

#include <lwip/api.h>
#include <tuple>

ConnectionMgr::ConnectionMgr() {
	// TODO Auto-generated constructor stub

}

ConnectionMgr::~ConnectionMgr() {
	// TODO Auto-generated destructor stub
}

void ConnectionMgr::callback(struct netconn* conn, enum netconn_evt enumNetconn_evt, u16_t len) {
	EventDispatcher::pushConnEvt(conn, enumNetconn_evt);
}

void ConnectionMgr::handleEvent(::netconn* conn, int evt)
{
	auto it = connections_.find(conn);
	if (connections_.end() != it)
	{
		switch (evt)
		{
		case NETCONN_EVT_RCVMINUS:
			break;
		case NETCONN_EVT_RCVPLUS:
			it->second->fetchData();
			break;
		case NETCONN_EVT_SENDPLUS:
			break;
		case NETCONN_EVT_SENDMINUS:
			break;
		case NETCONN_EVT_ERROR:
			break;
		default:
			break;
		}
	}
}

TcpListener* ConnectionMgr::createTcpListener(uint16_t port) {
	TcpListener* connection = nullptr;

	netconn* conn = netconn_new_with_callback(NETCONN_TCP, &ConnectionMgr::callback);
	if (nullptr != conn)
	{
		connection = new (std::nothrow) TcpListener(*this, conn);
		if (nullptr != connection)
		{
			connections_.insert(std::make_pair(conn, connection));

			netconn_set_nonblocking(conn, 1);
			netconn_bind(conn, NULL, port);
			netconn_listen(conn);
		}
		else
		{
			netconn_delete(conn);
			conn = 0;
		}
	}

	return connection;
}

TcpConnection* ConnectionMgr::createTcpConnection(netconn* conn) {
	TcpConnection* connection = nullptr;

	if (nullptr != conn)
	{
		connection = new (std::nothrow) TcpConnection(*this, conn);
		if (nullptr != connection)
		{
			connections_.insert(std::make_pair(conn, connection));

			netconn_set_nonblocking(conn, 1);
		}
		else
		{
			netconn_delete(conn);
			conn = 0;
		}
	}
	return connection;
}

TcpConnection* ConnectionMgr::createTcpConnection(ip_addr_t ip, uint16_t port) {
	TcpConnection* connection = nullptr;

	netconn* conn = netconn_new_with_callback(NETCONN_TCP, &ConnectionMgr::callback);
	if (nullptr != conn)
	{
		connection = new (std::nothrow) TcpConnection(*this, conn);
		if (nullptr != connection)
		{
			connections_.insert(std::make_pair(conn, connection));

			netconn_set_nonblocking(conn, 1);
			connection->connect(ip, port);
		}
		else
		{
			netconn_delete(conn);
			conn = 0;
		}
	}
	return connection;
}

UdpConnection* ConnectionMgr::createUdpConnection(ip_addr_t ip, uint16_t port) {
	UdpConnection* connection = nullptr;

	netconn* conn = netconn_new_with_callback(NETCONN_UDP, &ConnectionMgr::callback);
	if (nullptr != conn)
	{
		connection = new (std::nothrow) UdpConnection(*this, conn);
		if (nullptr != connection)
		{
			connections_.insert(std::make_pair(conn, connection));

			netconn_set_nonblocking(conn, 1);
			connection->connect(ip, port);
		}
		else
		{
			netconn_delete(conn);
			conn = 0;
		}
	}

	return connection;
}

void ConnectionMgr::removeTcpListener(TcpListener* listener) {
	if (nullptr != listener)
	{
		auto it = connections_.find(listener->getNetconn());
		if (connections_.end() != it)
		{
			delete listener;
			connections_.erase(it);
		}
	}
}

void ConnectionMgr::removeTcpConnection(TcpConnection* connection) {
	if (nullptr != connection)
	{
		auto it = connections_.find(connection->getNetconn());
		if (connections_.end() != it)
		{
			delete connection;
			connections_.erase(it);
		}
	}
}

void ConnectionMgr::removeUdpConnection(UdpConnection* connection) {
	if (nullptr != connection)
	{
		auto it = connections_.find(connection->getNetconn());
		if (connections_.end() != it)
		{
			delete connection;
			connections_.erase(it);
		}
	}
}
