/*
 * TcpListener.cpp
 *
 *  Created on: 11.07.2017
 *      Author: Slawomir Ciesielski
 */

#include "ConnMgr/TcpListener.h"
#include <lwip/tcp.h>

TcpListener::TcpListener(ConnectionMgr& connMgr) :
	connMgr_(connMgr)
{
	// TODO Auto-generated constructor stub

}

TcpListener::TcpListener(ConnectionMgr& connMgr, netconn* conn) :
	Connection(conn),
	connMgr_(connMgr)
{
	// TODO Auto-generated constructor stub

}

TcpListener::~TcpListener() {
	// TODO Auto-generated destructor stub
}

ip_addr_t TcpListener::getRemoteIp() const {
	Connection::conn_->pcb.tcp->remote_ip;
}

uint16_t TcpListener::getRemotePort() const {
	Connection::conn_->pcb.tcp->remote_port;
}

ip_addr_t TcpListener::getLocalIp() const {
	Connection::conn_->pcb.tcp->local_ip;
}

uint16_t TcpListener::getLocalPort() const {
	Connection::conn_->pcb.tcp->local_port;
}

void TcpListener::fetchData() {
	netconn* newConn = nullptr;
	netconn_accept(conn_, &newConn);

	TcpConnection* connection = connMgr_.createTcpConnection(newConn);
	GASSERT(acceptHandler_);
	acceptHandler_(connection);
}

