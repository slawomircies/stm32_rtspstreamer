/*
 * UdpConnection.cpp
 *
 *  Created on: 11.07.2017
 *      Author: Slawomir Ciesielski
 */

#include "ConnMgr/UdpConnection.h"
#include <lwip/udp.h>

UdpConnection::UdpConnection(ConnectionMgr& connMgr) :
	connMgr_(connMgr)
{
	// TODO Auto-generated constructor stub

}

UdpConnection::UdpConnection(ConnectionMgr& connMgr, netconn* conn) :
	Connection(conn),
	connMgr_(connMgr)
{
	// TODO Auto-generated constructor stub

}

UdpConnection::~UdpConnection() {
	// TODO Auto-generated destructor stub
}

err_t UdpConnection::connect(ip_addr_t ip, uint16_t port) {
	err_t result = netconn_connect(conn_, &ip, port);
	setState((result == ERR_OK) ? State::CONNECTED : State::DISCONECTED);
	return result;
}

err_t UdpConnection::disconnect() {
	err_t result = netconn_disconnect(conn_);
	setState((result == ERR_OK) ? State::DISCONECTED: State::INVALID);
	return result;
}

std::size_t UdpConnection::send(const char* buf, std::size_t size) {
	netbuf* connbuf = netbuf_new();
	netbuf_ref ( connbuf, buf, size );
	std::size_t result = (ERR_OK == netconn_send ( conn_, connbuf ))? size : 0;
	netbuf_delete( connbuf );
	return result;
}

void UdpConnection::fetchData() {
	netbuf* buf = nullptr;
	netconn_recv(conn_, &buf);

	GASSERT(recvHandler_);
	recvHandler_(buf);
	netbuf_delete(buf);
}

ip_addr_t UdpConnection::getRemoteIp() const {
	return conn_->pcb.udp->remote_ip;
}

uint16_t UdpConnection::getRemotePort() const {
	return conn_->pcb.udp->remote_port;
}

ip_addr_t UdpConnection::getLocalIp() const {
	return conn_->pcb.udp->local_ip;
}

uint16_t UdpConnection::getLocalPort() const {
	return conn_->pcb.udp->local_port;
}


