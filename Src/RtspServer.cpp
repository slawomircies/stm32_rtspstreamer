/*
 * RtspServer.cpp
 *
 *  Created on: 09.07.2017
 *      Author: Slawomir Ciesielski
 */

#include "RtspServer.h"
#include "lwip/api.h"


#include "ConnMgr/UdpConnection.h"
#include "ConnMgr/TcpConnection.h"
#include "ConnMgr/TcpListener.h"

RtspServer::RtspServer() :
	outBuf_(connRtsp_),
	outStream_(outBuf_),
	timer_()
{
	// TODO Auto-generated constructor stub

}

RtspServer::~RtspServer() {
	// TODO Auto-generated destructor stub
}

void RtspServer::start(uint16_t port) {

	connListener_ = connMgr_.createTcpListener(port);
	eventDispatcher_.registerAudioDevice(&audioDevice_);
	eventDispatcher_.registerConnectionMgr(&connMgr_);

	if (nullptr != connListener_)
	{
		connListener_->listen([this](TcpConnection* newConnection)
				{
					if (nullptr != newConnection)
					{
						printf("New RTSP connection\n");
						connRtsp_ = newConnection;
						connRtsp_->setRecvHandler(std::bind(
								&RtspServer::processRequest,
								this,
								std::placeholders::_1));
					}
				});
	}

	eventDispatcher_.run();
}

void RtspServer::processRequest(netbuf* buf) {
	if (nullptr != buf)
	{
		processRTSP(buf);
	}
	else
	{
		printf("NULL BUFFER\n");
		closeConnection();
	}
}

void RtspServer::closeConnection() {

	//timer_.stop();

	printf("Deinitialize Audio\n");
	audioDevice_.stopRecording();
//	audioDevice_.deinit();

	printf("Disconnecting RTCP connection\n");
	connRtcp_->disconnect();

	printf("Disconnecting RTP connection\n");
	connRtp_->disconnect();

	printf("Removing RTCP connection\n");
	connMgr_.removeUdpConnection(connRtcp_);

	printf("Removing RTP connection\n");
	connMgr_.removeUdpConnection(connRtp_);

	connRtcp_ = nullptr;
	connRtp_ = nullptr;

	printf("Disconnecting RTSP connection\n");
	connRtsp_->disconnect();

	printf("Removing RTSP connection\n");
	connMgr_.removeTcpConnection(connRtsp_);
	connRtsp_ = nullptr;

	rtspSession_.stop();
}

err_t RtspServer::processRTSP(netbuf* buf) {
	err_t result = ERR_OK;
	void *p;
	u16_t len;

	result = netbuf_data(buf, &p, &len);

	if (ERR_OK != result)
	{
		printf("INVALID BUFFER\n");
		return result;
	}

	std::map<std::string,std::string> header;
	Method 	methodId = Method::UNKNOWN;
	Version versionId = Version::UNKNOWN;

	std::string data(reinterpret_cast<char *>(p), len);

	header    = processRtspHeader(data);
	methodId  = processRtspMethod(header["method"]);
	versionId = processRtspVersion(header["version"]);

	printf("Processing RTSP request\n");

	if ((Method::UNKNOWN != methodId) && (Version::UNKNOWN != versionId) && (std::string::npos == data.find("\r\n\r\n")))
	{
		result = ERR_INPROGRESS;
	}
	else if ((Method::UNKNOWN == methodId) || (Version::UNKNOWN == versionId))
	{
		result = ERR_VAL;
	}

	if ((result == ERR_OK) && (Version::RTSP_1_0 == versionId))
	{
		switch (methodId)
		{
		case Method::DESCRIBE:
			{
				int status = 200;

				const char SDP[] = 	"m=audio 0 RTP/AVP 97\r\n"
									"a=rtpmap:97 speex/16000\r\n"
									"a=fmtp:97 mode=3;ptime=20\r\n";
									//"m=audio 0 RTP/AVP 10\r\n";

				outStream_	<< "RTSP/1.0 " << status << " " << getStatusName(status) << "\r\n"
							<< "CSeq: " << header["CSeq"] << "\r\n"
							<< "Content-Type: application/sdp" << "\r\n"
							<< "Content-Length: " << sizeof(SDP)-1 << "\r\n"
							<< "\r\n"
							<< SDP;

				outStream_.flush();
			}
			break;
		case Method::OPTIONS:
			{
				int status = 200;

				outStream_	<< "RTSP/1.0 " << status << " " << getStatusName(status) << "\r\n"
							<< "CSeq: " << header["CSeq"] << "\r\n"
							<< "Public:";

				auto opt = METHODS.begin();
				outStream_  << opt->first;
				++opt;

				for (;opt != METHODS.end(); ++opt)
				{
					outStream_ <<  ", " << opt->first;
				}

				outStream_ << "\r\n\r\n";

				outStream_.flush();
			}
			break;
		case Method::PLAY:
			{
				if (RtspSession::State::READY == rtspSession_.getState())
				{
					int status = 200;

					outStream_	<< "RTSP/1.0 " << status << " " << getStatusName(status) << "\r\n"
								<< "CSeq: " << header["CSeq"] << "\r\n"
								<< "Session: " << header["Session"] << "\r\n";

					outStream_ << "\r\n";

					outStream_.flush();
					audioDevice_.init();

					vad_.init();

					audioDevice_.record([&](int16_t* data, std::size_t len) {
						std::vector<uint8_t> encodedData;

						float VD = 0.0;
						float VDthr = 0.0;
						uint32_t vad_start = HAL_GetTick();
						bool vad = vad_.process(data, len, &VD);
						VDthr = vad_.getThr();
						uint32_t vad_stop = HAL_GetTick();

						printf("Vad time: %d ms; VD: %f; VD Threshold: %f\n", vad_stop-vad_start, VD, VDthr);

						if (true == vad)
						{
							uint32_t en_start = HAL_GetTick();
							encodedData = speexEncoder_.encode(data, len);
							uint32_t en_stop = HAL_GetTick();

							printf("Encoding time: %d ms\n", en_stop-en_start);
						}
						else
						{
							encodedData = std::vector<uint8_t>(1, 0);
						}

						if (encodedData.size())
						{
							rtspSession_.sendRtpFrame(std::move(encodedData));
						}
					});

					rtspSession_.setState(RtspSession::State::PLAYING);
				}
				else
				{
					int status = 455;

					outStream_	<< "RTSP/1.0 " << status << " " << getStatusName(status) << "\r\n"
								<< "CSeq: " << header["CSeq"] << "\r\n"
								<< "Allow: DESCRIBE, OPTIONS, TEARDOWN" << "\r\n";

					outStream_ << "\r\n";

					outStream_.flush();
				}
			}
			break;
		case Method::SETUP:
			{
				if (RtspSession::State::INIT == rtspSession_.getState())
				{
					int status = 200;

					std::string transport = header["Transport"];
					transport += ";";

					std::string transport_spec;
					getline(transport, transport_spec, 0, ';');
					if ("RTP/AVP" != transport_spec && "RTP/AVP/UDP" != transport_spec)
					{
						status = 461;
					}

					outStream_	<< "RTSP/1.0 " << status << " " << getStatusName(status) << "\r\n"
													<< "CSeq: " << header["CSeq"] << "\r\n"
													<< "Session: " << 12345678 << "\r\n";

					if (200 == status)
					{
						uint16_t rtcpPort = 0;
						uint16_t rtpPort = 0;

						std::size_t found = transport.find("client_port=");
						if (found != std::string::npos)
						{
							std::string port;
							found = getline(transport, port, found, '=');

							found = getline(transport, port, found, '-');
							rtpPort = atoi(port.c_str());

							found = getline(transport, port, found, ';');
							rtcpPort = atoi(port.c_str());
						}

						connRtp_  = connMgr_.createUdpConnection(connRtsp_->getRemoteIp(), rtpPort);
						connRtcp_ = connMgr_.createUdpConnection(connRtsp_->getRemoteIp(), rtcpPort);

						connRtp_->setRecvHandler(std::bind(&RtspSession::processRTP, &rtspSession_, std::placeholders::_1));
						connRtcp_->setRecvHandler(std::bind(&RtspSession::processRTCP, &rtspSession_, std::placeholders::_1));

						rtspSession_.setup(connRtp_, connRtcp_);

						printf("Local ports: rtp %d; rtcp %d\n", connRtp_->getLocalPort(), connRtcp_->getLocalPort());
						printf("Remote ports: rtp %d; rtcp %d\n", connRtp_->getRemotePort(), connRtcp_->getRemotePort());

						outStream_	<< "Transport: RTP/AVP/UDP;unicast;client_port=" << connRtp_->getRemotePort() << "-" << connRtcp_->getRemotePort()
																  << ";server_port=" << connRtp_->getLocalPort()  << "-" << connRtcp_->getLocalPort()
																  << ";ssrc=11223344" << "\r\n";

						rtspSession_.setState(RtspSession::State::READY);

					}

					outStream_ << "\r\n";

					outStream_.flush();
				}
				else
				{
					int status = 455;

					outStream_	<< "RTSP/1.0 " << status << " " << getStatusName(status) << "\r\n"
								<< "CSeq: " << header["CSeq"] << "\r\n"
								<< "Allow: PLAY, DESCRIBE, OPTIONS, TEARDOWN" << "\r\n";


					outStream_ << "\r\n";

					outStream_.flush();
				}
			}
			break;
		case Method::TEARDOWN:
			{
				int status = 200;

				outStream_	<< "RTSP/1.0 " << status << " " << getStatusName(status) << "\r\n"
							<< "CSeq: " << header["CSeq"] << "\r\n"
							<< "Session: " << header["Session"] << "\r\n";


				outStream_ << "\r\n";

				outStream_.flush();

				closeConnection();
			}
			break;
		case Method::GET_PARAMETER:
			{
				int status = 200;

				outStream_	<< "RTSP/1.0 " << status << " " << getStatusName(status) << "\r\n"
							<< "CSeq: " << header["CSeq"] << "\r\n"
							<< "Session: " << header["Session"] << "\r\n";

				outStream_ << "\r\n";

				outStream_.flush();

				rtspSession_.sendRtcpReport();
			}
			break;
		default:
			break;
		}
	}
	else if (ERR_OK == result)
	{
		int status = 505;

		outStream_	<< "RTSP/1.0 " << status << " RTSP Version not supported" << "\r\n";

		outStream_  << "\r\n";

		outStream_.flush();
	}

	printf("RTSP request result: %d\n", result);

    return result;
}


size_t RtspServer::getline(const std::string& src, std::string& dst, size_t pos, char delim)
{
	size_t npos = src.find(delim, pos);
	if (std::string::npos != npos)
	{
		dst = src.substr(pos, npos-pos);
		++npos;
	}
	return npos;
}

std::map<std::string,std::string> RtspServer::processRtspHeader(const std::string& request) {
	std::map<std::string,std::string> header;
	std::string m_field;
	size_t pos = 0;

	pos = getline(request, m_field, pos,' ');
	header.insert(std::make_pair("method", std::move(m_field)));

	pos = getline(request, m_field, pos,' ');
	header.insert(std::make_pair("uri", std::move(m_field)));

	pos = getline(request, m_field, pos,'\r');
	header.insert(std::make_pair("version", std::move(m_field)));

	++pos; 	// line ending

	for (std::string each; (std::string::npos != (pos = getline(request, each, pos,'\r'))) && (!each.empty()); ++pos /*line ending*/ )
	{
		std::size_t pos = each.find(":");
		header.insert(std::make_pair(each.substr(0,pos),each.substr(each.find_first_not_of(" ",pos+1))));
	};

	return header;
}

typename RtspServer::Method RtspServer::processRtspMethod(const std::string& value) {
	Method result = Method::UNKNOWN;

	auto it = METHODS.find(value);
	if (METHODS.end() != it)
	{
		result = it->second;
	}

	printf("RTSP method %s (%d)\n", value.c_str(), result);

	return result;
}

typename RtspServer::Version RtspServer::processRtspVersion(const std::string& value) {
	Version result = Version::UNKNOWN;
	auto it = VERSIONS.find(value);
	if (VERSIONS.end() != it)
	{
		result = it->second;
	}

	printf("RTSP version %s (%d)\n", value.c_str(), result);

	return result;
}

const char* RtspServer::getStatusName(int status) {
	const char* result = "Unknown";

	auto it = HTTP_STATUS.find(status);
	if (HTTP_STATUS.end() != it)
	{
		result = it->second;
	}

	return result;
}
