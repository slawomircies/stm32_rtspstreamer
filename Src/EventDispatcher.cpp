/*
 * EventHandler.cpp
 *
 *  Created on: 26.08.2017
 *      Author: Slawomir Ciesielski
 */

#include "EventDispatcher.h"

osMailQId  EventDispatcher::mail_box_;

EventDispatcher::EventDispatcher() {
	// TODO Auto-generated constructor stub

}

EventDispatcher::~EventDispatcher() {
	// TODO Auto-generated destructor stub
}

void EventDispatcher::run() {
	bool running = true;

	mail_box_ = osMailCreate(osMailQ(mail_box_), NULL);

    while (running) {
        osEvent evt = osMailGet(mail_box_, osWaitForever);
        if (evt.status == osEventMail) {
        	evt_t *mail = (evt_t*)evt.value.p;

        	switch (mail->type)
        	{
        	case EVT_CONN:
        	{
        		if (nullptr != connectionMgr_)
        		connectionMgr_->handleEvent(mail->data.evt_conn.netconn_,
        				mail->data.evt_conn.evt_);
        		break;
        	}
        	case EVT_TIM:
        	{
        		mail->data.evt_tim.timer_->execute();
        		break;
        	}
        	case EVT_AUDIO:
        	{
        		if (nullptr != audioDevice_)
        		audioDevice_->handleEvent(mail->data.evt_audio.evt_);
        		break;
        	}
        	case EVT_STOP:
        	{
        		running = false;
        		break;
        	}
        	default:
        	{
        		running = false;
        	}
        	}

            osMailFree(mail_box_, mail);
        }
        else
        {
        	printf("Event loop FAILED! %d", evt.status);
        	running = false;
        }
    }
}

void EventDispatcher::stop() {
	evt_t *mail = (evt_t*)osMailAlloc(mail_box_, osWaitForever);

	mail->type = EVT_STOP;

	osMailPut(mail_box_, mail);
}


void EventDispatcher::pushConnEvt(struct netconn* conn, enum netconn_evt enumNetconn_evt)
{
	evt_t *mail = (evt_t*)osMailAlloc(mail_box_, osWaitForever);
	if (nullptr != mail)
	{
		mail->type = EvtType::EVT_CONN;
		mail->data.evt_conn.netconn_ = conn;
		mail->data.evt_conn.evt_ = enumNetconn_evt;

		osMailPut(mail_box_, mail);
	}
	else
	{
		printf("pushConnEvt: Cannot allocate mail!\n");
	}
}

void EventDispatcher::pushTimEvt(Timer* timer)
{
	evt_t *mail = (evt_t*)osMailAlloc(mail_box_, osWaitForever);
	if (nullptr != mail)
	{
		mail->type = EvtType::EVT_TIM;
		mail->data.evt_tim.timer_ = timer;

		osMailPut(mail_box_, mail);
	}
	else
	{
		printf("pushTimEvt: Cannot allocate mail!\n");
	}
}

void EventDispatcher::pushAudioEvt(int evt)
{
	evt_t *mail = (evt_t*)osMailAlloc(mail_box_, osWaitForever);
	if (nullptr != mail)
	{
		mail->type = EvtType::EVT_AUDIO;
		mail->data.evt_audio.evt_ = evt;

		osMailPut(mail_box_, mail);
	}
	else
	{
		printf("pushAudioEvt: Cannot allocate mail!\n");
	}
}

void EventDispatcher::registerAudioDevice(AudioDevice* device) {
	audioDevice_ = device;
}

void EventDispatcher::registerConnectionMgr(ConnectionMgr* manager) {
	connectionMgr_ = manager;
}
