/*
 * RtspServer.h
 *
 *  Created on: 09.07.2017
 *      Author: Slawomir Ciesielski
 */

#ifndef RTSPSERVER_H_
#define RTSPSERVER_H_

#include <cstdint>
#include <map>

#include <lwip/err.h>
#include <lwip/netbuf.h>

#include <embxx/io/OutStream.h>
#include <OutStreamBuf.h>

#include "ConnMgr/ConnectionMgr.h"

#include "RtspSession.h"
#include "AudioGenerator.h"
#include "SpeexEncoder.h"
#include "AudioDevice.h"
#include "EventDispatcher.h"
#include "Timer.h"
#include "Vad.h"

class RtspServer {
public:
	enum class Version {
		UNKNOWN,
		RTSP_1_0
	};

	enum class Method {
		UNKNOWN,
		DESCRIBE,
		OPTIONS,
		PLAY,
		SETUP,
		TEARDOWN,
		GET_PARAMETER
	};

	const std::map<std::string, Method> METHODS = {{"DESCRIBE", Method::DESCRIBE},
			{"OPTIONS", Method::OPTIONS},
			{"PLAY", Method::PLAY},
			{"SETUP", Method::SETUP},
			{"TEARDOWN", Method::TEARDOWN},
			{"GET_PARAMETER", Method::GET_PARAMETER}};
	const std::map<std::string, Version> VERSIONS = {{"RTSP/1.0", Version::RTSP_1_0}};
	const std::map<int, const char*> HTTP_STATUS = {{200, "OK"},
												 	{455, "Method Not Valid In This State"},
													{461, "Unsupported transport"},
													{505, "RTSP Version not supported"}
												   };

	static const std::size_t OutStreamBufSize = 1024;
	typedef embxx::io::OutStreamBuf<TcpConnection, OutStreamBufSize> OutStreamBuf;
	typedef embxx::io::OutStream<OutStreamBuf> OutStream;

	typedef embxx::container::StaticQueue<int16_t, 2*640*2*3> AudioBuf;

	RtspServer();
	virtual ~RtspServer();

	void start(uint16_t port);
	void closeConnection();

private:
	void processRequest(netbuf* buf);
	err_t processRTSP(netbuf* buf);

	size_t getline(const std::string& src, std::string& dst, size_t pos, char delim);
	std::map<std::string,std::string> processRtspHeader(const std::string& request);
	Method processRtspMethod(const std::string& value);
	Version processRtspVersion(const std::string& value);
	const char* getStatusName(int status);

	EventDispatcher eventDispatcher_;

	RtspSession rtspSession_;

	ConnectionMgr connMgr_;
	TcpListener*   connListener_;
	TcpConnection* connRtsp_;
	UdpConnection* connRtcp_;
	UdpConnection* connRtp_;

	OutStreamBuf outBuf_;
	OutStream outStream_;

	AudioBuf audioBuf_;

	AudioGenerator audioSource_;
	AudioDevice audioDevice_;
	SpeexEncoder speexEncoder_;
	Vad vad_;

	Timer timer_;

};

#endif /* RTSPSERVER_H_ */
