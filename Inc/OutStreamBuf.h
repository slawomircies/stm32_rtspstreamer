//
// Copyright 2013 (C). Alex Robenko. All rights reserved.
//

// This file is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include <cstddef>
#include <algorithm>

#include "embxx/container/StaticQueue.h"
#include "embxx/util/StaticFunction.h"
#include "embxx/error/ErrorStatus.h"

namespace embxx
{

namespace io
{

/// @addtogroup io
/// @{

/// @brief Output Stream Buffer
/// @details This class implements functionality of output stream buffer
///          without use of dynamic memory allocation, RTTI and/or exceptions.
/// @tparam TDriver Class of the driver (such as embxx::driver::Character), it
///         must provide the following API functions and types:
///         @code
///         // Character type
///         typedef ... CharType;
///
///         // Asynchronous write request
///         template <typename TFunc>
///         void asyncWrite(const CharType* buf, std::size_t size, TFunc&& func);
///
///         // Get reference to event loop object
///         EventLoop& eventLoop();
///         @endcode
/// @tparam TBufSize Size of the internal buffer in number of characters it
///         may contain.
/// @tparam TWaitHandler Callback functor class to be called when requested
///         space becomes available. Must be either
///         std::function or embxx::util::StaticFunction and have
///         "void (const embxx::error::ErrorStatus&)" signature. It is used to store
///         callback handler provided in asyncWaitAvailableCapacity() request.
/// @pre No other components performs asynchronous write requests to the same
///      driver.
/// @headerfile embxx/io/OutStreamBuf.h
template <typename TWriteHandler,
		  std::size_t TBufSize,
		  typename TChartype = char>
class OutStreamBuf
{
public:
    /// @brief Type of the driver
    typedef TWriteHandler WriteHandler;

    /// @brief Type of single character
    typedef TChartype CharType;

    /// @brief Size of the internal buffer
    static const std::size_t BufSize = TBufSize;

    /// @brief Type of the internal circular buffer
    typedef embxx::container::StaticQueue<CharType, BufSize> Buffer;

    /// @brief Type of the iterator
    typedef typename Buffer::Iterator Iterator;

    /// @brief Same as Iterator
    typedef Iterator iterator;

    /// @brief Type of const Iterator
    typedef typename Buffer::ConstIterator ConstIterator;

    /// @brief Same as ConstIterator
    typedef ConstIterator const_iterator;

    /// @brief Type of values (characters) storred in internal buffer
    typedef typename Buffer::ValueType ValueType;

    /// @brief Same as ValueType
    typedef ValueType value_type;

    /// @brief Reference type
    typedef typename Buffer::Reference Reference;

    /// @brief Same as Reference
    typedef Reference reference;

    /// @brief Const reference type
    typedef typename Buffer::ConstReference ConstReference;

    /// @brief Same as ConstReference
    typedef ConstReference const_reference;

    /// @brief Constructor
    /// @param driv Reference to driver object
    explicit OutStreamBuf(WriteHandler*& writeHandler);

    /// @brief Destructor
    ~OutStreamBuf();

    /// @brief Copy constructor is default
    OutStreamBuf(const OutStreamBuf&) = default;

    /// @brief Move constructor is default
    OutStreamBuf(OutStreamBuf&&) = default;

    /// @brief Copy assignment operator is deleted
    OutStreamBuf& operator=(const OutStreamBuf&) = delete;

    /// @brief Get size of modifiable section in the internal buffer
    std::size_t size() const;

    /// @brief Check whether size of the modifiable section is 0.
    /// @return true if and only if size() == 0.
    bool empty() const;

    /// @brief Clear modifiable section of the internal buffer
    /// @post Call to size() returns 0;
    void clear();

    /// @brief Get current available space in the buffer until it becomes full.
    std::size_t availableCapacity() const;

    /// @brief Get full capacity of the buffer
    /// @details Equals to TSizeBuf template parameter of the class
    constexpr std::size_t fullCapacity() const;

    /// @brief Resize available for modification part of the buffer
    std::size_t resize(std::size_t newSize);

    /// @brief Flush the contents of the buffer to be written to the device.
    /// @param flushSize Number of characters from the "modifiable" part of the
    ///        buffer to be flushed to the device.
    /// @pre flushSize must be less or equal to the value returned by size()
    ///      prior to this function call.
    /// @post The "flushed" part of the buffer becomes unaccessible via
    ///       operator[], begin(), end() functions.
    /// @post The "flushed" part of the buffer mustn't be modified by any
    ///       external means until the write is complete.
    void flush(std::size_t flushSize);

    /// @brief Flush the whole buffer.
    /// @details Equivalent to flash(size()).
    void flush();

    /// @brief Push back "C" (0-terminated) string
    /// @details The final 0 is not pushed.
    /// @param str 0 terminated string
    /// @return Number of characters written, may be less than length of the
    ///         string (in case the internal buffer becomes full).
    std::size_t pushBack(const CharType* str);

    /// @copydoc pushBack(const CharType*)
    std::size_t push_back(const CharType* str);

    /// @brief Push back buffer of characters.
    /// @param str Pointer to buffer of characters
    /// @param strSize Size of the buffer
    /// @return Number of characters written, may be less than strSize parameter
    ///         (in case the internal buffer becomes full).
    std::size_t pushBack(const CharType* str, std::size_t strSize);

    /// @copydoc pushBack(const CharType*, std::size_t)
    std::size_t push_back(const CharType* str, std::size_t strSize);

    /// @brief Push back single character.
    /// @param ch Character
    /// @return 1 in case of success, 0 in case internal buffer is full
    std::size_t pushBack(CharType ch);

    /// @copydoc pushBack(CharType)
    std::size_t push_back(CharType ch);

    /// @brief Returns iterator to the beginning of "modifiable" (not flushed)
    ///        section in the buffer.
    Iterator begin();

    /// @brief Returns iterator to the end of "modifiable" (not flushed)
    ///        section of the buffer
    Iterator end();

    /// @brief Same as cbegin()
    ConstIterator begin() const;

    /// @brief Same as cend()
    ConstIterator end() const;

    /// @brief Const version of begin()
    ConstIterator cbegin() const;

    /// @brief Const version of end()
    ConstIterator cend() const;

    /// @brief Operator to access element in the "modifiable" (not flushed)
    ///        section of the buffer.
    /// @param idx Index of the element.
    /// @pre @code idx < size() @endcode
    Reference operator[](std::size_t idx);

    /// @brief Const version of operator[]
    ConstReference operator[](std::size_t idx) const;


private:

    void initiateFlush();

    Buffer buf_;
    std::size_t flushedSize_;
    std::size_t waitAvailableCapacity_;
    WriteHandler*& writeHandler_;
};

/// @}

// Implementation
template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
OutStreamBuf<TWriteHandler, TBufSize, TChartype>::OutStreamBuf(WriteHandler*& writeHandler)
    : flushedSize_(0),
      waitAvailableCapacity_(0),
	  writeHandler_(writeHandler)
{
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
OutStreamBuf<TWriteHandler, TBufSize, TChartype>::~OutStreamBuf()
{
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
std::size_t OutStreamBuf<TWriteHandler, TBufSize, TChartype>::size() const
{
    return buf_.size() - flushedSize_;
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
bool OutStreamBuf<TWriteHandler, TBufSize, TChartype>::empty() const
{
    return (size() == 0U);
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
void OutStreamBuf<TWriteHandler, TBufSize, TChartype>::clear()
{
    buf_.popBack(size());
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
std::size_t OutStreamBuf<TWriteHandler, TBufSize, TChartype>::availableCapacity() const
{
    return buf_.capacity() - flushedSize_;
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
constexpr std::size_t
OutStreamBuf<TWriteHandler, TBufSize, TChartype>::fullCapacity() const
{
    return buf_.capacity();
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
std::size_t OutStreamBuf<TWriteHandler, TBufSize, TChartype>::resize(
    std::size_t newSize)
{
    auto minSize = std::min(fullCapacity(), newSize + flushedSize_);
    buf_.resize(minSize);
    return size();
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
void OutStreamBuf<TWriteHandler, TBufSize, TChartype>::flush(
    std::size_t flushSize)
{
    GASSERT(flushSize <= size());
    bool flushRequired = (flushedSize_ == 0);
    std::size_t actualFlushSize = std::min(flushSize, size());
    flushedSize_ += actualFlushSize;
    if (flushRequired) {
        initiateFlush();
    }
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
void OutStreamBuf<TWriteHandler, TBufSize, TChartype>::flush()
{
    flush(size());
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
std::size_t OutStreamBuf<TWriteHandler, TBufSize, TChartype>::pushBack(
    const CharType* str)
{
    std::size_t count = 0;
    while ((*str != static_cast<CharType>(0) && (!buf_.isFull()))) {
        pushBack(*str);
        ++str;
        ++count;
    }
    return count;
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
std::size_t OutStreamBuf<TWriteHandler, TBufSize, TChartype>::push_back(
    const CharType* str)
{
    return pushBack(str);
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
std::size_t OutStreamBuf<TWriteHandler, TBufSize, TChartype>::pushBack(
    const CharType* str, std::size_t strSize)
{
    auto sizeToWrite = std::min(strSize, buf_.capacity() - buf_.size());
    for (auto count = 0U; count < sizeToWrite; ++count) {
        buf_.pushBack(*str);
        ++str;
    }
    return sizeToWrite;
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
std::size_t OutStreamBuf<TWriteHandler, TBufSize, TChartype>::push_back(
    const CharType* str, std::size_t strSize)
{
    return pushBack(str, strSize);
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
std::size_t OutStreamBuf<TWriteHandler, TBufSize, TChartype>::pushBack(
    CharType ch)
{
    if (buf_.isFull()) {
        return 0;
    }
    buf_.pushBack(ch);
    return 1;
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
std::size_t OutStreamBuf<TWriteHandler, TBufSize, TChartype>::push_back(
    CharType ch)
{
    return pushBack(ch);
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
typename OutStreamBuf<TWriteHandler, TBufSize, TChartype>::Iterator
OutStreamBuf<TWriteHandler, TBufSize, TChartype>::begin()
{
    return buf_.begin() + flushedSize_;
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
typename OutStreamBuf<TWriteHandler, TBufSize, TChartype>::Iterator
OutStreamBuf<TWriteHandler, TBufSize, TChartype>::end()
{
    return buf_.end();
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
typename OutStreamBuf<TWriteHandler, TBufSize, TChartype>::ConstIterator
OutStreamBuf<TWriteHandler, TBufSize, TChartype>::begin() const
{
    return cbegin();
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
typename OutStreamBuf<TWriteHandler, TBufSize, TChartype>::ConstIterator
OutStreamBuf<TWriteHandler, TBufSize, TChartype>::end() const
{
    return cend();
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
typename OutStreamBuf<TWriteHandler, TBufSize, TChartype>::ConstIterator
OutStreamBuf<TWriteHandler, TBufSize, TChartype>::cbegin() const
{
    return buf_.cbegin() + flushedSize_;
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
typename OutStreamBuf<TWriteHandler, TBufSize, TChartype>::ConstIterator
OutStreamBuf<TWriteHandler, TBufSize, TChartype>::cend() const
{
    return buf_.cend();
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
typename OutStreamBuf<TWriteHandler, TBufSize, TChartype>::Reference
OutStreamBuf<TWriteHandler, TBufSize, TChartype>::operator[](std::size_t idx)
{
    GASSERT(idx < size());
    return buf_[idx + flushedSize_];
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
typename OutStreamBuf<TWriteHandler, TBufSize, TChartype>::ConstReference
OutStreamBuf<TWriteHandler, TBufSize, TChartype>::operator[](
    std::size_t idx) const
{
    GASSERT(idx < size());
    return buf_[idx + flushedSize_];
}

template <typename TWriteHandler, std::size_t TBufSize, typename TChartype>
void OutStreamBuf<TWriteHandler, TBufSize, TChartype>::initiateFlush()
{
    static const std::size_t MaxSingleFlushSize = BufSize / 4;
    auto rangeOne = buf_.arrayOne();
    auto distance = static_cast<std::size_t>(
        std::distance(rangeOne.first, rangeOne.second));
    auto flushSize = std::min(std::size_t(MaxSingleFlushSize), distance);
    flushSize = std::min(flushSize, flushedSize_);

    std::size_t bytesWritten = writeHandler_->send(&buf_.front(), flushSize);

	GASSERT(bytesWritten <= flushedSize_);
	GASSERT(bytesWritten <= buf_.size());
	flushedSize_ -= bytesWritten;
	buf_.popFront(bytesWritten);

	if (0 < flushedSize_) {
		initiateFlush();
	}
}

}  // namespace io

}  // namespace embxx


