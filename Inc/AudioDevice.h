/*
 * AudioRecorder.h
 *
 *  Created on: 19.07.2017
 *      Author: Slawomir Ciesielski
 */

#ifndef AUDIODEVICE_H_
#define AUDIODEVICE_H_

#include <embxx/util/StaticFunction.h>


struct DefaultAudioTraits
{
    /// @brief The "read" handler storage type.
    /// @details It is the default version of embxx::util::StaticFunction.
    /// @pre The handler must have the following signature:
    ///      @code void handler(const embxx::error::ErrorStatus&, std::size_t); @endcode
    typedef embxx::util::StaticFunction<void(int16_t* , std::size_t)> ReadHandler;

    /// @brief The "write" handler storage type.
    /// @details It is the default version of embxx::util::StaticFunction.
    /// @pre The handler must have the following signature:
    ///      @code void handler(const embxx::error::ErrorStatus&, std::size_t); @endcode
    typedef embxx::util::StaticFunction<void(int16_t* , std::size_t)> WriteHandler;

    /// @brief Read queue size
    /// @details By default the Character driver supports only single read
    ///          at a time.
    static const std::size_t ReadQueueSize = 2*320; //2*20ms 16kHz

    /// @brief Write queue size
    /// @details By default the Character driver supports only single write
    ///          at a time.
    static const std::size_t WriteQueueSize = 2*320; //2*20ms 32kHz
};

class AudioDevice {
public:

	typedef DefaultAudioTraits Traits;

    /// @brief Type of read handler holder class
    typedef typename Traits::ReadHandler ReadHandler;

    /// @brief Type of write handler holder class.
    typedef typename Traits::WriteHandler WriteHandler;

    /// @brief Maximum number of pending asynchronous read requests.
    static const std::size_t ReadQueueSize = Traits::ReadQueueSize;

    /// @brief Maximum number of pending asynchronous write requests.
    static const std::size_t WriteQueueSize = Traits::WriteQueueSize;

    enum audio_evt
	{
    	AUDIO_EVT_RXCPLT,
    	AUDIO_EVT_RXHALF,
    	AUDIO_EVT_TXCPLT,
    	AUDIO_EVT_TXHALF
	};

	AudioDevice();
	virtual ~AudioDevice();

	void init();
	void deinit();

    template <typename TFunc>
    void record(TFunc&& func)
    {
    	isRecording_ = true;
    	recordInfo_.handler_ = std::forward<TFunc>(func);
    	recordInfo_.start_ = inBuff_;
    	recordInfo_.current_ = inBuff_;
    	startRead(inBuff_, ReadQueueSize);
    }


    template <typename TFunc>
    void play(TFunc&& func)
    {
    	isPlaying_ = true;
    	playInfo_.handler_ = std::forward<TFunc>(func);
    	playInfo_.start_ = outBuff_;
    	playInfo_.current_ = outBuff_;
    	startWrite(outBuff_, WriteQueueSize);
    }

	void stopRecording();
	void stopPlaying();

    void handleEvent(int evt);

private:
    struct PlayInfo
    {
    	PlayInfo()
        : start_(nullptr),
          current_(nullptr),
          bufSize_(0)
        {
        }

        template <typename THandlerParam>
        PlayInfo(
        	int16_t* start,
            std::size_t bufSize,
            THandlerParam&& handler)
            : start_(start),
              current_(start),
              bufSize_(bufSize),
              handler_(std::forward<THandlerParam>(handler))
        {
        }

        int16_t* start_;
        int16_t* current_;
        std::size_t bufSize_;
        WriteHandler handler_;
    };

    struct RecordInfo
    {
    	RecordInfo()
            : start_(nullptr),
              current_(nullptr),
              bufSize_(0)
        {
        }

        template <typename THandlerParam, typename TPredParam>
        RecordInfo(
			uint16_t* buf,
            std::size_t bufSize,
            THandlerParam&& handler)
            : start_(buf),
              current_(buf),
              bufSize_(bufSize),
              handler_(std::forward<THandlerParam>(handler))
        {
        }

        int16_t* start_;
        int16_t* current_;
        std::size_t bufSize_;
        ReadHandler handler_;
    };

    void startRead(int16_t* buff, std::size_t length);
    void startWrite(int16_t* buff, std::size_t length);

    void writeCompleteHandler();
    void writeHalfHandler();
    void readCompleteHandler();
    void readHalfHandler();

    static int16_t outBuff_[WriteQueueSize];
    static int16_t inBuff_[ReadQueueSize];

    RecordInfo recordInfo_;
    PlayInfo   playInfo_;

    bool isPlaying_;
    bool isRecording_;
	
};

#endif /* AUDIODEVICE_H_ */
