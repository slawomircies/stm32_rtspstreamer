/*
 * Timer.h
 *
 *  Created on: 17.07.2017
 *      Author: Slawomir Ciesielski
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <chrono>
#include <embxx/util/StaticFunction.h>
#include <cmsis_os.h>

class Timer
{
public:
	typedef embxx::util::StaticFunction<void (void)> TimeoutHandler;

	Timer();
	~Timer();

	template <typename TRep, typename TPeriod, typename TFunc>
	void start(
		const std::chrono::duration<TRep, TPeriod>& waitTime,
		TFunc&& func,
		bool oneShot = true)
	{
		handler_ = std::forward<TFunc>(func);
		timer_ = osTimerCreate(osTimer(timeout_), oneShot ? osTimerOnce : osTimerPeriodic, this);
		osTimerStart(timer_, std::chrono::milliseconds(waitTime).count());
	}

	void stop();
	void execute();


private:
	TimeoutHandler handler_;

	void timeout();
	static void timeout(const void* obj);

	osTimerDef(timeout_, Timer::timeout);
	osTimerId timer_;
};

#endif /* TIMER_H_ */
