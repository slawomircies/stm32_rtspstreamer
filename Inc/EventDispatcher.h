/*
 * EventHandler.h
 *
 *  Created on: 26.08.2017
 *      Author: Slawomir Ciesielski
 */

#ifndef EVENTDISPATCHER_H_
#define EVENTDISPATCHER_H_


#include <AudioDevice.h>
#include <ConnMgr/ConnectionMgr.h>

class EventDispatcher {
public:
	EventDispatcher();
	~EventDispatcher();

	static void pushConnEvt(struct netconn* conn, enum netconn_evt enumNetconn_evt);
	static void pushTimEvt(Timer* timer);
	static void pushAudioEvt(int evt);

	void registerAudioDevice(AudioDevice* device);
	void registerConnectionMgr(ConnectionMgr* manager);

	void run();
	void stop();

private:
	AudioDevice* audioDevice_;
	ConnectionMgr* connectionMgr_;

	enum EvtType
	{
		EVT_STOP,
		EVT_CONN,
		EVT_TIM,
		EVT_AUDIO
	};

	struct evt_t {
		EvtType type;

		union {
			struct {
				::netconn*  netconn_;
				int evt_;
			} evt_conn;
			struct {
				::Timer*  timer_;
			} evt_tim;
			struct {
				int evt_;
			} evt_audio;
		} data;
	};

	osMailQDef(mail_box_, 32, evt_t);
	static osMailQId  mail_box_;
};

#endif /* EVENTDISPATCHER_H_ */
