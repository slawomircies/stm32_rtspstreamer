/*
 * Vad.h
 *
 *  Created on: 31.08.2017
 *      Author: Slawomir Ciesielski
 */

#ifndef VAD_H_
#define VAD_H_

#include <stm32f7xx_hal.h>
#include <vector>
#include <arm_math.h>
#include <embxx/container/StaticQueue.h>

class Vad {
public:
	Vad();
	~Vad();

	void init();
	bool process(int16_t* data, std::size_t len, float *param = 0);
	float getThr() const;

private:
	static const size_t trainTime_ = 30;

    typedef embxx::container::StaticQueue<float, trainTime_> Buffer;

	arm_rfft_fast_instance_f32 S_;
	size_t trainCnt_;

	size_t hangOverNs_;
	size_t hangOverS_;

	const size_t hangOverNsCnt_;
	const size_t hangOverSCnt_;

	bool prevVad_;
	float thr_;
	Buffer Bns_;
	Buffer Bs_;

	bool hangOver(bool vad);
	void updateThr(bool vad, float VD);
	void initThr();
	float computeVD(int16_t* data, std::size_t len);
};

#endif /* VAD_H_ */
