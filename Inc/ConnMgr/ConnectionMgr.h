/*
 * ConnectionMgr.h
 *
 *  Created on: 11.07.2017
 *      Author: Slawomir Ciesielski
 */

#ifndef CONNECTIONMGR_H_
#define CONNECTIONMGR_H_


#include <ConnMgr/Connection.h>
#include <Timer.h>
#include <lwip/api.h>
#include <cmsis_os.h>
#include <map>

class TcpListener;
class TcpConnection;
class UdpConnection;

class ConnectionMgr {
public:
	ConnectionMgr();
	virtual ~ConnectionMgr();

	static void callback (struct netconn *conn, enum netconn_evt, u16_t len);

    void handleEvent(::netconn* conn, int evt);

	TcpListener* createTcpListener(uint16_t port);

	TcpConnection* createTcpConnection(netconn* conn);
	TcpConnection* createTcpConnection(ip_addr_t ip, uint16_t port);
	UdpConnection* createUdpConnection(ip_addr_t ip, uint16_t port);

	void removeTcpListener(TcpListener* listener);
	void removeTcpConnection(TcpConnection* connection);
	void removeUdpConnection(UdpConnection* connection);
private:
	std::map<::netconn*, Connection*> connections_;
};

#endif /* CONNECTIONMGR_H_ */
