/*
 * UdpConnection.h
 *
 *  Created on: 11.07.2017
 *      Author: Slawomir Ciesielski
 */

#ifndef CONNMGR_UDPCONNECTION_H_
#define CONNMGR_UDPCONNECTION_H_

#include <ConnMgr/Connection.h>

class ConnectionMgr;

class UdpConnection: public Connection {
public:

	typedef embxx::util::StaticFunction<void (netbuf* buf)> ReceiveHandler;

	UdpConnection(ConnectionMgr& connMgr);
	UdpConnection(ConnectionMgr& connMgr, netconn* conn);
	virtual ~UdpConnection();

	err_t connect(ip_addr_t ip, uint16_t port);
	err_t disconnect();
	std::size_t send(const char* buf, std::size_t size);

	void fetchData();

	ip_addr_t getRemoteIp() const;
	uint16_t  getRemotePort() const;
	ip_addr_t getLocalIp() const;
	uint16_t  getLocalPort() const;

	template<typename TFunc>
	void setRecvHandler(TFunc&& func);

private:
	ConnectionMgr& connMgr_;
	ReceiveHandler recvHandler_;
};


template<typename TFunc>
void UdpConnection::setRecvHandler(TFunc&& func) {
	recvHandler_ = std::forward<TFunc>(func);
}
#endif /* CONNMGR_UDPCONNECTION_H_ */
