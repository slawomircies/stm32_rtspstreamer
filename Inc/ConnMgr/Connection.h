/*
 * Connection.h
 *
 *  Created on: 11.07.2017
 *      Author: Slawomir Ciesielski
 */

#ifndef Connection_H_
#define Connection_H_

#include <lwip/api.h>
#include <embxx/util/StaticFunction.h>

class Connection {
public:
	enum class State {
		DISCONECTED,
		CONNECTED,
		LISTEN,
		INVALID
	};

	Connection();
	Connection(netconn* conn);
	Connection(const Connection& o);
	Connection& operator=(Connection&& o);

	virtual ~Connection();


	virtual void fetchData()=0;

	State getState() const;
	netconn* getNetconn() const;
	virtual ip_addr_t getRemoteIp() const = 0;
	virtual uint16_t  getRemotePort() const = 0;
	virtual ip_addr_t getLocalIp() const = 0;
	virtual uint16_t  getLocalPort() const = 0;

protected:
	void setState(State state);
	netconn* conn_;

private:
	State state_;
};

#endif // Connection_H_
