/*
 * TcpListener.h
 *
 *  Created on: 11.07.2017
 *      Author: Slawomir Ciesielski
 */

#ifndef CONNMGR_TCPLISTENER_H_
#define CONNMGR_TCPLISTENER_H_

#include <ConnMgr/Connection.h>
#include <ConnMgr/ConnectionMgr.h>
#include <ConnMgr/TcpConnection.h>


class TcpListener: public Connection {
public:
	TcpListener(ConnectionMgr& connMgr);
	TcpListener(ConnectionMgr& connMgr, netconn* conn);
	virtual ~TcpListener();

	typedef embxx::util::StaticFunction<void (TcpConnection*)> AcceptHandler;

	template<typename TFunc>
	err_t listen(TFunc&& func);

	void fetchData();

	ip_addr_t getRemoteIp() const;
	uint16_t  getRemotePort() const;
	ip_addr_t getLocalIp() const;
	uint16_t  getLocalPort() const;

private:
	ConnectionMgr& connMgr_;
	AcceptHandler acceptHandler_;
};

template<typename TFunc>
err_t TcpListener::listen(TFunc&& func) {
	setState(State::LISTEN);

	acceptHandler_ = std::forward<TFunc>(func);

	return ERR_OK;
}

#endif /* CONNMGR_TCPLISTENER_H_ */
