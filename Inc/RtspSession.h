/*
 * RtspSession.h
 *
 *  Created on: 10.07.2017
 *      Author: Slawomir Ciesielski
 */

#ifndef RTSPSESSION_H_
#define RTSPSESSION_H_

#include <cstdint>
#include <vector>
#include "lwip/ip_addr.h"
#include "lwip/pbuf.h"

#include "ConnMgr/UdpConnection.h"

class RtspSession {
public:
	RtspSession();
	virtual ~RtspSession();

	enum class State {
		INIT = 0,
		READY = 1,
		PLAYING = 2
	};

	struct ntp_t {
		uint32_t intpart;
		uint32_t fractpart;
	};

	void setup(UdpConnection* connRtp, UdpConnection* connRtcp);
	void pushData(const std::vector<uint8_t>& data);
	void stop();

	ntp_t convertToNtp(uint32_t miliseconds);

	uint16_t  getRemoteRtcpPort();
	uint16_t  getLocalRtcpPort();

	uint16_t  getRemoteRtpPort();
	uint16_t  getLocalRtpPort();

	void  processRTCP(netbuf* buf);
	void  processRTP(netbuf* buf);

	void  sendRtpFrame(const std::vector<uint8_t>& data);
	void  sendRtcpReport();

	State getState();
	void setState(const State& state);

private:
	State state_;
	uint16_t rtcpPort_;
	uint16_t rtpPort_;

	size_t packetsSent_;
	size_t octetsSent_;

	UdpConnection* connRtp_;
	UdpConnection* connRtcp_;
};

#endif /* RTSPSESSION_H_ */
