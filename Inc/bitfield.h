/*
 * bitfield.h
 *
 *  Created on: 29.05.2017
 *      Author: Slawomir Ciesielski
 */

#ifndef UTILS_BITFIELD_H_
#define UTILS_BITFIELD_H_


namespace bitfield {
	template<int POS, int SIZE, typename T>
    T get(T data) {
        return ((data) >> POS) & ((1ULL << SIZE)-1);
    }

	template<int POS, int SIZE, typename T>
    void set(T& data, int x) {
        T mask( ((1ULL << SIZE)-1) << POS );
        data = (data & ~mask) | ((x << POS) & mask);
    }
}



#endif /* UTILS_BITFIELD_H_ */
