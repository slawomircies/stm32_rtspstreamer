#ifndef __RTPPACKET__
#define __RTPPACKET__

#include <vector>
#include <embxx/util/Assert.h>
#include <bitfield.h>
#include <stdint.h>


class RtpPacket
{
public:
	RtpPacket();
	~RtpPacket();

	void setHeader(std::vector<uint32_t>& data);
	uint8_t* getHeader();

	uint32_t 	getVersion();
	void 		setVersion(uint32_t val);

	uint32_t 	getPadding();
	void 		setPadding(uint32_t val);

	uint32_t 	geteXtension();
	void 		seteXtension(uint32_t val);

	uint32_t 	getCsrcCount();
	void 		setCsrcCount(uint32_t val);

	uint32_t 	getMarker();
	void 		setMarker(uint32_t val);

	uint32_t 	getPayloadType();
	void 		setPayloadType(uint32_t val);

	uint32_t 	getSequenceNumber();
	void 		setSequenceNumber(uint32_t val);

	uint32_t 	getTimeStamp();
	void 		setTimeStamp(uint32_t val);

	uint32_t 	getSSRC();
	void 		setSSRC(uint32_t val);

private:
	uint32_t header_[3];
};

#endif //__RTPPACKET__
