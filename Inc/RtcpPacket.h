#ifndef __RTCPPACKET__
#define __RTCPPACKET__

#include <vector>
#include <algorithm>
#include <iterator>
#include <embxx/util/Assert.h>
#include <bitfield.h>
#include <stdint.h>


struct RtcpSourceDescriptionItem {
	uint8_t data_[1];

	uint8_t 	getType();
	void 		setType(uint8_t val);

	uint8_t 	getLength();
	void 		setLength(uint8_t val);

	uint8_t* 	getData();
	void 		setData(uint8_t* val, size_t len);

	RtcpSourceDescriptionItem*	getItemNext();
};

struct RtcpSourceDescription {
	uint32_t data_[2];

	uint32_t 	getSSRC();
	void 		setSSRC(uint32_t val);

	RtcpSourceDescriptionItem* 	getItem();
};

struct RtcpSenderInfo {
	uint32_t data_[5];

	uint32_t 	getNTPTimestampHi();
	void 		setNTPTimestampHi(uint32_t val);

	uint32_t 	getNTPTimestampLo();
	void 		setNTPTimestampLo(uint32_t val);

	uint32_t 	getRTPTimestamp();
	void 		setRTPTimestamp(uint32_t val);

	uint32_t 	getPacketCount();
	void 		setPacketCount(uint32_t val);

	uint32_t 	getOctetCount();
	void 		setOctetCount(uint32_t val);
};

struct RtcpReportBlock {
	uint32_t data_[6];

	uint32_t 	getSSRC();
	void 		setSSRC(uint32_t val);

	uint32_t 	getFractLost();
	void 		setFractLost(uint32_t val);

	uint32_t 	getPacketsLost();
	void 		setPacketsLost(uint32_t val);

	uint32_t 	getHiSeqRcvd();
	void 		setHiSeqRcvd(uint32_t val);

	uint32_t 	getJitter();
	void 		setJitter(uint32_t val);

	uint32_t 	getLastSR();
	void 		setLastSR(uint32_t val);

	uint32_t 	getDelayLastSR();
	void 		setDelayLastSR(uint32_t val);

	RtcpReportBlock* 	getReportBlockNext();
};

class RtcpPacket
{
public:
	RtcpPacket();
	~RtcpPacket();

	void setHeader(uint32_t data);
	uint8_t* getHeader();

	uint32_t 	getVersion();
	void 		setVersion(uint32_t val);

	uint32_t 	getPadding();
	void 		setPadding(uint32_t val);

	uint32_t 	getCount();
	void 		setCount(uint32_t val);

	uint32_t 	getPayloadType();
	void 		setPayloadType(uint32_t val);

	uint32_t 	getLength();
	void 		setLength(uint32_t val);

	uint32_t 	getSSRCSender();
	void 		setSSRCSender(uint32_t val);

	RtcpReportBlock* 		getReportBlock();
	RtcpSenderInfo* 		getSenderInfo();
	RtcpSourceDescription*	getSourceDescription();

private:
	uint32_t header_[2];
};
#endif //__RtcpPacket__
