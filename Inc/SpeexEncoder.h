/*
 * SpeexEncoder.h
 *
 *  Created on: 23.07.2017
 *      Author: Slawomir Ciesielski
 */

#ifndef SPEEXENCODER_H_
#define SPEEXENCODER_H_

#include <speex.h>
#include <vector>

class SpeexEncoder {
public:
	SpeexEncoder();
	virtual ~SpeexEncoder();

public:
	std::vector<uint8_t> encode(int16_t* data, std::size_t len);
	std::size_t getFrameSize();

private:
	void *speexObj_;
	SpeexBits speexBits_;
};

#endif /* SPEEXENCODER_H_ */
